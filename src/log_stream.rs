extern crate rdkafka;
use rdkafka::producer::{FutureProducer, FutureRecord};
use rdkafka::util::Timeout;
use rdkafka::consumer::{BaseConsumer, Consumer};
use rdkafka::message::Message;
use ethcontract::{H256, RawLog};
use std::io::Write;
use std::io::{Cursor, Read};




// Implement the ToBytes trait for the RawLog struct
impl ToBytes for ethcontract::RawLog {
    fn to_bytes(&self) -> Result<Vec<u8>, std::io::Error> {
        let mut bytes = Vec::new();

        // Serialize the topics
        for topic in &self.topics {
            bytes.write_all(&topic.as_bytes())?;
        }

        // Serialize the data
        bytes.write_all(&self.data)?;

        Ok(bytes)
    }
}

// Define the ToBytes trait
trait ToBytes {
    fn to_bytes(&self) -> Result<Vec<u8>, std::io::Error>;
}

impl FromBytes for ethcontract::RawLog {
    fn from_bytes(bytes: &[u8]) -> Result<Self, std::io::Error> {
        let mut reader = Cursor::new(bytes);
        let mut topics = Vec::new();
        let mut data = Vec::new();

        while let Ok(topic_bytes) = read_exact(&mut reader, 32) {
            let topic = H256::from_slice(&topic_bytes);
            topics.push(topic);
        }

        reader.read_to_end(&mut data)?;

        Ok(ethcontract::RawLog { topics, data })
    }
}

fn read_exact(reader: &mut impl Read, len: usize) -> Result<Vec<u8>, std::io::Error> {
    let mut buffer = vec![0u8; len];
    reader.read_exact(&mut buffer)?;
    Ok(buffer)
}

trait FromBytes: Sized {
    fn from_bytes(bytes: &[u8]) -> Result<Self, std::io::Error>;
}


// Function to send log data to Kafka
pub async fn data_in(producer: FutureProducer, log_topic: &str, log_value: ethcontract::RawLog, log_key: &str) {
    // Convert log_value to bytes
    let bytes = log_value.to_bytes().unwrap();

    let record = FutureRecord::to(log_topic).payload(&bytes).key(log_key);

    let timeout = Timeout::Never;
    
    producer.send(record, timeout).await.unwrap();
}


// Function to receive log data from Kafka
pub async fn data_out(consumer: BaseConsumer, log_topic: &str) -> ethcontract::RawLog {
    let timeout = std::time::Duration::from_secs(5);

    consumer.subscribe(&[log_topic]).expect("Failed to subscribe to topic");

    let message_result = consumer.poll(timeout).unwrap();

    let message = message_result.unwrap();

    let payload_bytes = message.payload();

    ethcontract::RawLog::from_bytes(payload_bytes.unwrap()).unwrap()    

}