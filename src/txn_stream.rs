extern crate rdkafka;
use rdkafka::producer::{FutureProducer, FutureRecord};
use rdkafka::util::Timeout;
use rdkafka::consumer::{BaseConsumer, Consumer};
use rdkafka::message::Message;

pub async fn data_in(producer: FutureProducer, txn_topic: &str, txn_value: &[u8], txn_key: &str) {

    let record = FutureRecord::to(txn_topic).payload(txn_value).key(txn_key);

    let timeout = Timeout::Never;

    producer.send(record, timeout).await.unwrap();
}

pub async fn data_out(consumer: BaseConsumer, txn_topic: &str) -> [u8; 32] {
    let timeout = std::time::Duration::from_secs(5);
    consumer
        .subscribe(&[txn_topic])
        .expect("Failed to subscribe to topic");
     
    let message_result = consumer.poll(timeout).unwrap();

    let message = message_result.unwrap();

    let payload_bytes = message.payload().unwrap();

    payload_bytes.try_into().unwrap()
}
